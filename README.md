![Mailinq App Logo](client/public/img/mailinq_logo_readme.svg)

#### [Live Demo](http://mailinq.bkevans.com) - Be patient...this app may be slow to start due to being deployed on Heroku

A POC application built React and Redux that allows users to generate, send, and report on binary email surveys. This app contains both the React front-end, and a NodeJS back-end server. Authentication is handled by a Google OAuth 2.0 integration with Passport and Express. Credits can be "purchased" through a Stripe test account integration., MongoDB is used for data storage, and SendGrid is used as an email service. 

The front-end was bootstrapped with Create-React-App as a starting point, and is located in the *client/* directory. Styling is written in Sass and is built on top of the Materialize CSS framework. Webpack handles compiling the code into a browser-friendly bundle with the help of Babel and node-sass, and is as configured with CRA.

## Getting Started

### Prerequisites

Make sure that you have [Node.js](https://nodejs.org/en/) and [Yarn](https://yarnpkg.com/en/) installed.

To check the versions you have installed, run:  

```
node -v && yarn -v
```

### Installing

First you'll need to clone the source files onto your local machine. 

To clone you can run:

```
git clone https://bkevans@bitbucket.org/bkevans/mailinq-app.git
```

Once cloned, move into the app directory:

```
cd mailinq-app/
```

Now you'll need to install of the necessary dependencies to run the app locally using Yarn.

**Important:** you'll need to install dependencies for both the server, and the client. There are two differentpackage.json files, one for the server in the root directory, and one for the client app in the client/ directory.

First install the server dependencies in the root directory:

```
yarn install
```

Next move into the client directory and installed the client app dependencies

```
cd client && yarn install
```

### Service Integration

In order to run the app locally, you'll need access keys for [Google](https://developers.google.com/identity/protocols/OAuth2), [Stripe](https://stripe.com/docs/development), and [SendGrid](https://sendgrid.com/docs/for-developers/), as well as to configure MongoDB storage. I run a local mongo instance for development. Create a file named dev.js within the server/config directory. This is where keys for the development environment are defined. 

*(A file named prod.js can also be created for production keys. the dev and prod key files are not tracked by git.)* 

In server/config/dev.js, define the following keys for your dev environment:
 
    module.exports = {
        googleClientID: '',
        googleClientSecret: '',
        mongoURI: 'ENTER_YOUR_MONGO_URI ex: mongodb://localhost:27017/MailinqDev',
        cookieKey: 'ENTER_A_RANDOM_STRING_OF_CHARACTERS',
        stripePublishableKey: '',
        stripeSecretKey: '',
        sendGridKey: '',
        redirectDomain: 'http://localhost:3000'
    }

You will also need to update the REACT_APP_STRIPE_KEY .env vars within the client directory. 

### Start the Dev Server

Once the environment variables are in place you're ready to run a development build and spin up a development server for the app by running the dev command from within the server directory. This will concurrently spin up both the server and the client: 

```
cd server && yarn dev
```

The app should spin up the development server and open your default browser to *localhost:3000*. **Note**: the express server is actually running on port 5000.  

Now that the app is up and running locally, you can crack open the *client/* directory and play around with the code to update components or styling. Changes will be rendered in the browser immediately as long as the development server is running. 

To shut down the dev server, simply press [cntrl + c] **twice**

## Deployment

This project uses the Heroku CLI for deploying the application to Heroku. If you'd like to host your own customized version of this app for free on Heroku, more information can be found in the [Heroku docs](https://devcenter.heroku.com/articles/how-heroku-works).

More information on git based deployments with the Heroku CLI can be found [here](https://devcenter.heroku.com/articles/heroku-cli). 

There is a script in the server *package.json* for deploying a production build to Heroku, which is utilized by the Heroku CLI. This is the **heroku-postbuild** script.


## Built With

* [React](https://reactjs.org/) - The UI library used
* [Create React App](https://facebook.github.io/create-react-app/) - To quickly bootstrap the app front-end
* [Redux](https://reactjs.org/) - Used for application state management
* [Sass](https://sass-lang.com/) - To help make things look interesting, easily
* [Materialize CSS](https://materializecss.com/) - A CSS framework built with Google's material design principles
* [Express](https://firebase.google.com/) - The web server framework
* [Passport](https://firebase.google.com/) - Middleware to easily connect to Google OAuth2.0
* [SendGrid](https://sendgrid.com/docs/for-developers/) - Email service provider
* [M Lab](https://mlab.com/) - MongoDB Database-as-a-Service

## License

This project is licensed under the MIT License

## Acknowledgments

This project was initially built while participating in an excellent [Udemy course](https://www.udemy.com/node-with-react-fullstack-web-development/) provided by Stephen Grider. A huge thank you to Stephen for sharing his knowledge and helping others grow their developement toolkit!