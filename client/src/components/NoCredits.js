import React from 'react';
import { Spring } from 'react-spring/renderprops';

export default function NoCredits() {
  return (
		<Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
			{props => (
				<div className="div__action-wrapper" style={props}>
					<h4>You have no credits or survey campaigns.</h4>
					<p>
						Add credits to your account by clicking the 'Add Credits' in the toolbar above.
					</p>
					<p>
						Since this app is a proof of concept, credits cost you
						nothing...which means surveys cost you nothing! Each survey
						campaign you create will use one credit.
					</p>
					<p>
						Use the following card information to purchase credits throught
						the Stripe test API:
					</p>
					<table className="striped table__card-table">
						<tbody>
							<tr>
								<td>Card Number:</td>
								<td>4242 4242 4242 4242</td>
							</tr>
							<tr>
								<td>Expiration Date:</td>
								<td>Any future date</td>
							</tr>
							<tr>
								<td>CVV:</td>
								<td>Any 3 digit value</td>
							</tr>
						</tbody>
					</table>
				</div>
			)}
		</Spring>
	);
}
