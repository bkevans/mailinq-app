import React, { Component } from 'react';
import { connect } from 'react-redux';
import Payments from './Payments';
import { Spring } from 'react-spring/renderprops';

class Credits extends Component {
	renderCredits() {
		if (this.props.auth) {
			return (
				<div className="div__credits container">
					<section className="section__greeting">
						<span>Hello {this.props.auth.firstName}!</span>
					</section>
					<section className="availableCredits">
						{this.props.auth.credits ?
              <div>
                <span>
                  Available Credits:
                  <Spring
                    from={{ opacity: 0, number: 0 }}
                    to={{ opacity: 1, number: this.props.auth.credits }}
                    delay={1000}
                  >
                    {props => <b style={props}> {Math.round(props.number)}</b>}
                  </Spring>
                </span>
                <Payments />
              </div>
              : 
              <Payments newUser="true"/> 
            }
					</section>
				</div>
			);
		}
	}

	render() {
		return <div className="div__credits-wrapper">{this.renderCredits()}</div>;
	}
}

function mapStateToProps({ auth }) {
  return { auth };
}

export default connect(mapStateToProps)(Credits);