import M from 'materialize-css/dist/js/materialize.min';
import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { ToastContainer } from 'react-toastify';

import ScrollIntoView from './ScrollIntoView';
import Header from './Header';
import Credits from './Credits';
import Landing from './Landing';
import Dashboard from './Dashboard';
import notFound from './notFound';
import SurveyNew from './surveys/SurveyNew';
import SurveyDetails from './surveys/SurveyDetails';
import Footer from './Footer';

class App extends Component {
	componentDidMount() {
		this.props.fetchUser();
		M.AutoInit();
	}

	render() {
		return (
			<div className="div__site">
				<BrowserRouter onUpdate={() => window.scrollTo(0, 0)}>
					<div className="div__site-content">
						<ScrollIntoView>
						<Header />
						<Credits />
						<Switch>
							<Route
								exact
								path="/"
								render={props =>
									this.props.auth ? (
										<Dashboard
											{...props}
											userCredits={this.props.auth.credits}
										/>
									) : (
										<Landing />
									)
								}
							/>
							<Route
								exact
								path="/surveys"
								render={props =>
									this.props.auth ? (
										<Dashboard
											{...props}
											userCredits={this.props.auth.credits}
										/>
									) : (
										'loading'
									)
								}
							/>
							<Route path="/surveys/new" component={SurveyNew} />
							<Route
								path="/surveys/:id"
								component={SurveyDetails}
							/>
							<Route path="*" component={notFound} />
						</Switch>
						</ScrollIntoView>
					</div>
				</BrowserRouter>
				<Footer />
				<ToastContainer autoClose={2000} />
			</div>
		);
	}
};

function mapStateToProps({ auth }) {
    return { auth };
} 

export default connect(mapStateToProps, actions)(App);