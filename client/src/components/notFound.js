import React from 'react';
import { Link } from 'react-router-dom';

export default () => {
  return (
    <div className="div__not-found-wrapper">
      <h4>
        Oh dang, <span>{window.location.href}</span> doesn't exist.
      </h4>
      <Link to="/" className="waves-effect waves-light btn light-blue darken-3 white-text">
        Mailinq Home
      </Link>
    </div>
  );
}