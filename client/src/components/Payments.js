import React, { Component } from 'react';
import StripeCheckout from 'react-stripe-checkout';
import { connect } from 'react-redux';
import * as actions from '../actions';

class Payments extends Component {
	removeButtonClass = (event) => {
		event.target.classList.remove('pulse');
	}
  render() {
    return (
			<StripeCheckout
				image="http://mailinq.bkevans.com/favicons/favicon-96x96.png"
				description="$5 for 5 campaign credits"
				allowRememberMe={false}
				amount={500}
				token={token => this.props.handleToken(token)}
				stripeKey={process.env.REACT_APP_STRIPE_KEY}
			>
				<button 
					className={`btn waves-effect waves-light blue btn-payment ${this.props.newUser ? 'pulse' : ''}`}
					onClick={this.removeButtonClass}
				>
					Add Credits
				</button>
			</StripeCheckout>
		);
  }
}

export default connect(null, actions)(Payments);