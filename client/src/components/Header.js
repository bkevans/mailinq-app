import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import logo from '../main_logo.svg';

class Header extends Component {
  renderContent() {
    switch (this.props.auth) {
      case null:
        return;
      case false: 
        return <li><a href="/auth/google">Login with Google</a></li>;
      default: 
        return [
          <li key="1"><Link to="/surveys">Dashboard</Link></li>,
          <li key="2"><a href="/api/logout"><i className="material-icons right top">exit_to_app</i>Logout</a></li>
        ];
    }
  }

  render() {
    return (
      <div className="navbar-fixed">
        <nav>
          <div className="nav-wrapper">
            <div className="container nav__container">
              <Link 
                to={this.props.auth ? '/surveys' : '/'} 
                className="brand-logo left"
              >
                <img src={logo} className="App-logo" alt="Mailinq Logo" />
              </Link>
              <ul className="right">
                {this.renderContent()}
              </ul>
            </div>
          </div>
        </nav>
      </div>
    )
  }
}

function mapStateToProps({ auth }) {
  return { auth };
} 

export default connect(mapStateToProps)(Header);