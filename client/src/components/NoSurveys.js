import React from 'react'
import { Link } from 'react-router-dom';
import { Spring } from 'react-spring/renderprops';

export default function noSurveys() {
  return (
		<div>
			<Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
				{props => (
					<div className="div__action-wrapper" style={props}>
						<h4>You currently have no survey campaigns.</h4>
						<p>
							Survey campaigns are created by clicking the big blue plus
							button at the bottom right of your screen.
						</p>
						<p>
							Each survey campaign you create will debit your account 1
							credit. Since this app is a POC, it doesn't actually cost you
							anything...but real email surveys are created and sent out.
						</p>
						<p>Click the plus button below to create a survey campaign.</p>
					</div>
				)}
			</Spring>
			<div className="fixed-action-btn">
				<Link
					to="/surveys/new"
					className="waves-effect waves-light btn-floating btn-large blue pulse"
					title="Create a New Survey"
				>
					<i className="material-icons">add</i>
				</Link>
			</div>
		</div>
	);
}
