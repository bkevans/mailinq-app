import React, { useEffect } from 'react';
import mockup from '../images/mockup.jpg';

const Landing = () => {
  useEffect(() => {
    document.body.classList.add('landing');
    return () => {
      document.body.classList.remove('landing');
    }
  }, []);  
  return (
		<div className="div__landing-wrapper">
			<div className="div__landing-banner">
				<div className="container div__banner-content">
					<h1>
						Collecting user feedback <br />
						has never been <strong>easier.</strong>
					</h1>
					<h2>
						We make it easy to send surveys and gather results, so you can
						focus on what matters most:
						<br />
						<strong>your customers.</strong>
					</h2>
				</div>
			</div>
			<div className="container div__landing-content">
				<div className="row div__intro-section">
          <div className="col m6 push-m6">						
            <h3>Long, boring surveys don’t work any more...</h3>
            <p>Who has time to answer a customer satisfaction survey these days?</p>
            <p>Don’t settle for sub-5% response rates. Mailinq is proven to reduce customer churn.</p>
          </div>
          <div className="col m6 pull-m6">
            <img src={mockup} className="App-logo" alt="Mailinq Logo" />
          </div>
				</div>
			</div>
			<p>Collect feedback from your users.</p>
		</div>
	);
}

export default Landing;