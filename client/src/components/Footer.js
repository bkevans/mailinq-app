import React from 'react';
import bkevans_logo from '../images/bkevans_logo.svg';

export default () => {
  return (
		<footer>
			<div className="div__footer">
				<div className="container">
					<p>
            For some other interesting internet things...
          </p>
          <p>
						<a
							href="http://www.bkevans.com"
							target="_blank"
							rel="noopener noreferrer"
            >
              visit
							<img
								src={bkevans_logo}
								className="App-logo"
								alt="Mailinq Logo"
							/>
							.com
						</a>
					</p>
          <small>
            &copy; {new Date().getFullYear()} Mailinq - BKEvans
          </small>
				</div>
			</div>
		</footer>
	);
};