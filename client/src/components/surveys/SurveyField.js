// Contains logic to render a single input
import React from 'react';

export default ({ input, label, meta: { error, touched, active } }) => {
  return (
    <div className="input-field">
      <input {...input} id={input.name}/>
      <label htmlFor={input.name} className={active || input.value.trim().length ? 'active' : undefined}>{label}</label>
      <span className="helper-text" data-error="wrong">{touched && error}</span>
    </div>
  );
};