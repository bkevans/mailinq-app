import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchSurveyDetails, deleteSurvey } from '../../actions';
import { Bar } from 'react-chartjs-2';
import { Spring, config } from 'react-spring/renderprops';

class SurveyDetails extends Component {
  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.fetchSurveyDetails(id);
  }

  onDeleteSurvey = () => {
    this.props.deleteSurvey(this.props.survey._id);
    this.props.history.push('/surveys');
  };

  renderChart() {
    return (
      <Bar 
        data={{
          labels: [`No: ${this.props.survey.no}`, `Yes: ${this.props.survey.yes}`],
          datasets: [{
            data: [this.props.survey.no, this.props.survey.yes],
            backgroundColor: ['#D7386D','#294661']
          }]
        }}
        options={{
          legend: { display: false },
          scales: { 
            yAxes: [{ ticks: { max: this.props.maxY, min: 0, stepSize: (this.props.maxY / 4) } }],
            xAxes: [{ ticks: { fontSize: 14 } }]
          },
          tooltips: { enabled: false },
          hover: { mode: null }
        }} 
      />
    );
  }

  renderSurveyDetails() {
    const detail = this.props.survey;
    let totalRecipients = detail.recipients ? detail.recipients.length : 0;
    const totalResponses = detail.yes + detail.no;
    return (
      <div className="div__survey-detail-item card">
        <div className="card-content">
          <div className="row" style={{ position:'relative' }}>
            <div className="col s12 m6 div__data-wrapper">
              <h6>Details</h6>
              <section>
                <p><strong>Date Sent:</strong> {new Date(detail.dateSent).toLocaleDateString()}</p>
                <p><strong>Subject:</strong> {detail.subject}</p>
                <p><strong>Email Body:</strong> {detail.body}</p>
              </section>
              <h6>Responses</h6>
              <section>
                <p><strong>Emails Sent:</strong> {totalRecipients}</p>
                <p><strong>Responses:</strong> {totalResponses}</p>
                <p><strong>Response Rate:</strong> {((totalResponses / totalRecipients) * 100).toFixed(2)}%</p>
                <p><strong>Last Response: </strong> 
                  {detail.lastResponded ? new Date(detail.lastResponded).toLocaleDateString() : 'N/A'}
                </p>
              </section>
            </div>
            <div className="col s12 m6">
              <h6 style={{marginBottom: '1rem'}}>Results</h6>
              {this.renderChart()}
              <button className="btn-flat btn-large transparent link__survey-delete" onClick={this.onDeleteSurvey}>
                <i className="large material-icons right">delete_forever</i> Delete Campaign
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
			<div className="div__content-wrapper survey-list">
				<div className="div__survey-details-wrapper container">
					<h5>
            <span>Campaign:</span>
            <Spring 
              from={{ opacity: 0, transform: 'translate3d(-50px,0,0)' }}
              to={{ opacity: 1, transform: 'translate3d(0,0,0)' }}
              config={config.wobbly}>
							{props => (
								<strong style={props}> {this.props.survey.title}</strong>
							)}
						</Spring>
					</h5>
					<Spring
						from={{ opacity: 0, transform: 'translate3d(0,10px,0)' }}
						to={{ opacity: 1, transform: 'translate3d(0,0,0)' }}
						config={config.wobbly}
					>
						{props => (
							<div style={props}>
								{this.renderSurveyDetails()}
								<Link to={'/surveys'}>
									<i className="material-icons top">list</i>
									&nbsp;View all Campaigns
								</Link>
							</div>
						)}
					</Spring>
					<div className="fixed-action-btn">
						<Link
							to="/surveys/new"
							className="waves-effect waves-light btn-floating btn-large blue"
							title="Create a new survey"
						>
							<i className="large material-icons">add</i>
						</Link>
					</div>
				</div>
			</div>
		);
  }
}

function mapStateToProps({ survey }) {
  return { survey };
}

export default connect(mapStateToProps, { fetchSurveyDetails, deleteSurvey })(SurveyDetails);
