import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Spring, Trail, config } from 'react-spring/renderprops';

class SurveyList extends Component {
	renderSurveys() {
		const surveyList = this.props.surveys;
		return (
			<Trail
				items={surveyList}
				from={{ opacity: 0, transform: 'translate3d(10px,10px,0)' }}
				to={{ opacity: 1, transform: 'translate3d(0,0,0)' }}
				keys={surveyList.map((survey, i) => i)}
				config={config.wobbly}
			>
				{survey => props => (
					<Link
						className="link__survey-list-item card"
						to={`/surveys/${survey._id}`}
						style={props}
						key={survey._id}
					>
						<div className="card-content">
							<div className="div__flex-content">
								<section className="section__card-title">
									<p className="card-title">{survey.title}</p>
									<p>
										<small>
											<i>Sent On:</i>{' '}
											{new Date(survey.dateSent).toLocaleDateString()}
										</small>
									</p>
								</section>
								<section className="card-responses">
									<p>
										Emails Sent: <strong>{survey.recipients.length}</strong>
									</p>
									<p>
										Responses: <strong>{survey.no + survey.yes}</strong>
									</p>
								</section>
							</div>
						</div>
					</Link>
				)}
			</Trail>
		);
	}

  render() {
    return (
			<div className="div__survey-list-wrapper container">
				<h5>
					<span>Campaigns:</span>
					<Spring
						from={{ opacity: 0, transform: 'translate3d(-50px,0,0)' }}
						to={{ opacity: 1, transform: 'translate3d(0,0,0)' }}
						config={config.wobbly}>
						{props => (
							<strong style={props}> {this.props.surveys.length}</strong>
						)}
					</Spring>
				</h5>
				{this.renderSurveys()}
				<div className="fixed-action-btn">
					<Link
						to="/surveys/new"
						className="waves-effect waves-light btn-floating btn-large blue"
						title="Create a New Survey"
					>
						<i className="material-icons">add</i>
					</Link>
				</div>
			</div>
		);
  }
}

export default SurveyList;

