import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import formFields from './formFields';
import * as actions from '../../actions';
import { Spring, config } from 'react-spring/renderprops';

const SurveyFormReview = ({ onCancel, formValues, submitSurvey, history }) => {
  const renderFields = formFields.map(({ name, label }) => {
    return (
      <div key={name}>
        <label>{label}:</label>
        <p>{formValues[name]}</p>
      </div>
    );
  });

  return (
		<Spring from={{ opacity: 0 }} to={{ opacity: 1 }} config={config.slow}>
			{props => (
				<div className="div__form-review-wrapper" style={props}>
					<h5>Please confirm your entries for this survey.</h5>
					<p>
						<i>
							You will be charged 1 credit for this email survey campaign.
						</i>
					</p>
					<div className="div_form-review-fields-wrapper">{renderFields}</div>
					<button
						className="btn left transparent btn-bigger btn-flat"
						onClick={onCancel}
					>
						<i className="material-icons left">chevron_left</i>
						Back
					</button>
					<button
						className="waves-effect waves-light btn right blue darken-1 white-text btn-bigger"
						onClick={() => submitSurvey(formValues, history)}
					>
						Send Survey
						<i className="material-icons right">send</i>
					</button>
				</div>
			)}
		</Spring>
	);
}

function mapStateToProps(state) {
  return {
    formValues: state.form.surveyForm.values
  };
}

export default connect(mapStateToProps, actions)(withRouter(SurveyFormReview));