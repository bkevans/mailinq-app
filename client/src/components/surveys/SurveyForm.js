// This component shows a form for a user to add input
import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { Link } from 'react-router-dom';
import SurveyField from './SurveyField';
import validateEmails from '../../utils/validateEmails';
import formFields from './formFields';
import { Spring, config } from 'react-spring/renderprops';

class SurveyForm extends Component {
  renderFields() {
    return formFields.map(({ label, name }) => 
      <Field key={name} component={SurveyField} type="text" label={label} name={name} />
    );
  }

  render() {
    return (
			<Spring
				from={{ opacity: 0 }}
				to={{ opacity: 1 }}
				config={config.slow}
			>
				{props => (
					<div className="div__form-wrapper" style={props}>
						<h5>Enter the details for this email survey campaign.</h5>
						<p>
							<i>Enter recipient emails as a comma separated list.</i>
						</p>
						<form
							onSubmit={this.props.handleSubmit(this.props.onSurveySubmit)}
						>
							{this.renderFields()}
							<Link
								to="/surveys"
								className="btn-flat transparent btn-bigger"
							>
								<i className="material-icons top">cancel</i>
								Cancel
							</Link>
							<button
								type="submit"
								className="waves-effect waves-light btn right blue darken-1 white-text btn-bigger"
							>
								Review
								<i className="material-icons right">chevron_right</i>
							</button>
						</form>
					</div>
				)}
			</Spring>
		);
  };
}

function validate(values) {
  const errors ={};

  errors.recipients = validateEmails(values.recipients || '');

  formFields.forEach(({ name, noValueError }) => {
      if (!values[name]) {
        errors[name] = noValueError;
      }
    });

  return errors;
}

export default reduxForm({
  validate,
  form: 'surveyForm',
  destroyOnUnmount: false
})(SurveyForm);