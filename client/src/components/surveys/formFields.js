export default [
  {
    label: 'Campaign Title',
    name: 'title',
    noValueError: 'Please provide a title for this campaign.'
  }, {
    label: 'Email Subject Line',
    name: 'subject',
    noValueError: 'Please provide a subject for the email.'
  }, {
    label: 'Email Yes/No Survey Question',
    name: 'body',
    noValueError: 'Please provide a survey question for the email.'
  }, {
    label: 'Recipient List',
    name: 'recipients',
    noValueError: 'You must provide at least one recipient.'
  }
]; 