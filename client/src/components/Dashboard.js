import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchSurveys } from '../actions';
import SurveyList from './surveys/SurveyList';
import NoCredits from './NoCredits';
import NoSurveys from './NoSurveys';

class Dashboard extends Component {
  componentDidMount() {
    this.props.fetchSurveys();
  }

  renderContent() {
    if ((!this.props.userCredits || this.props.userCredits === 0) && this.props.surveys.length === 0) {
			return <NoCredits />;
		} else if (this.props.surveys.length === 0) {
			return <NoSurveys />;
		} else {
      return <SurveyList surveys={this.props.surveys}/>;
		}
  }

  render() {
    return (
			<div className="div__content-wrapper dashboard-wrapper">
				{this.renderContent()}
			</div>
		);
  }
};

function mapStateToProps({ surveys }) {
  return { surveys };
}

export default connect(mapStateToProps, { fetchSurveys })(Dashboard);