import axios from 'axios';
import { toast } from 'react-toastify';
import { FETCH_USER } from './types';
import { FETCH_SURVEYS } from './types';
import { DELETE_SURVEY } from './types';
import { FETCH_SURVEY_DETAILS } from './types';

export const fetchUser = () => async dispatch => {
  const res = await axios.get('/api/current_user')
  dispatch({
    type: FETCH_USER,
    payload: res.data
  });
};

export const handleToken = token => async dispatch => {
  const res = await axios.post('/api/stripe', token)
  dispatch({
    type: FETCH_USER,
    payload: res.data
  });
}

export const submitSurvey = (values, history) => async dispatch => {
  const res = await axios.post('/api/surveys', values);

  history.push('/surveys');
  dispatch({ 
    type: FETCH_USER, 
    payload: res.data[0] 
  });
  toast(res.data[1].toast, {
    className: 'toast-message',
    closeButton: false,
    hideProgressBar: true
  });              
};

export const fetchSurveys = () => async dispatch => {
  const res = await axios.get('/api/surveys');

  dispatch({ type: FETCH_SURVEYS, payload: res.data });
};

export const deleteSurvey = (id) => async dispatch => {
  const res = await axios.get('/api/survey_delete', { params: { id } });

  dispatch({ type: DELETE_SURVEY, payload: res.data[0] });
  toast(res.data[1].toast, {
		className: 'toast-message',
		closeButton: false,
		hideProgressBar: true
	});              
};

export const fetchSurveyDetails = (id) => async dispatch => {
  const res = await axios.get('/api/survey_details', { params: { id } });

  dispatch({ type: FETCH_SURVEY_DETAILS, payload: res.data });
};