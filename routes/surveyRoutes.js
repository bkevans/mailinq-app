const _ = require('lodash');
const Path = require('path-parser').default;
const { URL } = require('url');
const mongoose = require('mongoose');
const requireLogin = require('../middlewares/requireLogin');
const requireCredits = require('../middlewares/requireCredits');
const Mailer = require('../services/Mailer');
const surveyTemplate = require('../services/emailTemplates/surveyTemplate');

const Survey = mongoose.model('surveys');

module.exports = app => {
  app.get('/api/surveys', requireLogin, async (req, res) => {
    const surveys = await Survey
      .find({ _user: req.user.id })
      .select();
    res.send(surveys);
  });

  app.get('/api/survey_details', requireLogin, async (req, res) => {
    const surveyDetail = await Survey
      .findOne({ _id: req.query.id })
      .select();
    res.send(surveyDetail);
  });

  app.get('/api/survey_delete', requireLogin, async (req, res) => {
    await Survey
      .find({ _user: req.user.id })
      .deleteOne({ _id: req.query.id });
    const surveys = await Survey
      .find({ _user: req.user.id })
      .select();
    res.send([surveys, { toast: 'Campaign Deleted' }]);
  });

  app.get('/api/surveys/:surveyId/:choice', (req, res) => {
    res.send('Thank you for your response!');
  });

  app.post('/api/surveys/webhooks', (req, res) => {
    const p = new Path('/api/surveys/:surveyId/:choice');

    // use lodash chain to chain the functions manipulating req.body
    const events = _.chain(req.body)
      // map over the array of destructured objects
      .map(({ email, url }) => {
        // see if the url contains surveyId and choice -> returns obj || null
        const match = p.test(new URL(url).pathname);
        if (match) {
          return {
            email,
            surveyId: match.surveyId, 
            choice: match.choice
          };
        }
      })
      // strip out undefined values
      .compact()
      // strip out duplicates -> email && surveyId
      .uniqBy('email', 'surveyId')
      // iterate over each event and update collection in DB
      .each( async ({ surveyId, email, choice }) => {
        // find and update exactly one record in the survey collection
        try {
          await Survey.updateOne({
            _id: surveyId,
            recipients: {
              $elemMatch: { email: email, responded: false }
            }
          }, {
            // use key interpolation to increment correct choice by 1
            $inc: { [choice]: 1 },
            // set the responded value for the appropriate recipient by index
            $set: { 'recipients.$.responded': true },
            // update the last responded property
            lastResponded: new Date()
          }).exec();
        } catch (e) {
          console.log(e);
          return;
        }
     })
      //return the filtered reponses
      .value()

    // send a response back to SendGrid
    res.status(200).send({all:'good'});
  });

  app.post('/api/surveys', requireLogin, requireCredits, async (req, res) => {
    const { title, subject, body, recipients } = req.body;

    const survey = new Survey({
      title,
      subject,
      body,
      recipients: recipients.split(',').map(email => ({ email: email.trim() })),
      _user: req.user.id,
      dateSent: Date.now()
    });

    // Send an email
    const mailer = new Mailer(survey, surveyTemplate(survey));

    try {
      await mailer.send();
      await survey.save();

      req.user.credits -= 1;
      const user = await req.user.save();

      res.send([user, { toast: 'Campaign Created' }]);
    } catch (err) {
      res.status(422).send(err);
    }
  });
};