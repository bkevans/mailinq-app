const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const mongoose = require('mongoose');
const keys = require('../config/keys');

const User = mongoose.model('users');

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  const user = await User.findById(id);
  done(null, user);
});

//create a new instance of the Google Strategy for passport to use
passport.use(
  new GoogleStrategy(
    {
      clientID: keys.googleClientID,
      clientSecret: keys.googleClientSecret,
      callbackURL: '/auth/google/callback',
      proxy: true
    },
    async (accessToken, refreshToken, profile, done) => {
      const exisitingUser = await User.findOne({ googleId: profile.id });
      if (exisitingUser) {
        return done(null, exisitingUser);
      }
      const user = await new User({
        googleId: profile.id,
        firstName: profile.name['givenName']
      }).save();
      done(null, user);
    }
  )
);