const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema({
  googleId: String,
  firstName: String,
  credits: {
    type: Number,
    default: null
  }
});

mongoose.model('users', userSchema);